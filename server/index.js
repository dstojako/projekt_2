require("dotenv").config();
const express = require('express');
const cors = require('cors');
const busBoy = require('busboy-body-parser');
const database = require("./config/database");

// Init ExpressJS
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(busBoy({ limit: '5mb' }));

// Initialize Database connection
database.connect();
// Init Models
require('./models');

// Routes
app.use('/api', require('./routes'));

const { PORT } = process.env;
// Start hosting
app.listen(PORT, () => console.log(`Server listening on port ${ PORT }`));
