const jwt = require('jsonwebtoken');
const { JWT_SECRET } = process.env;

module.exports = (req, res, next) => {
  if (!req.header('Authorization')) {
    return res.status(401).send("Unauthorized");
  }

  const [schema, token] = req.header('Authorization').split(' ');
  if (schema != 'Bearer' && !token) {
    return res.status(401).send("Unauthorized");
  }

  try {
    jwt.verify(token, JWT_SECRET, 
      (error, decoded) => {
        if (error) {
          console.error(error);
          return res.status(400).send("Invalid token");
        }
        req.user = { ...decoded };
        next();
    });
  } 
  catch (error) {
    console.error(error);
    return res.status(500).send("Internal server error");
  }
};
