const express = require('express');
const router = express.Router();

const AuthRoutes = require('./AuthRoutes');
const FileRoutes = require('./FileRoutes');

router.use('/auth', AuthRoutes);
router.use('/files', FileRoutes);

module.exports = router;
