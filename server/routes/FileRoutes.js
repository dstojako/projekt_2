const express = require('express');
const router = express.Router();

const AuthMiddleware = require('../middlewares/AuthMiddleware');
const FileController = require('../controllers/FileController');

router.route('/')
  .get(FileController.getAll)
  .post(AuthMiddleware, FileController.upload);

router.route('/download/:id').get(FileController.download);

router.route('/:id')
  .get(FileController.get)
  .put(AuthMiddleware, FileController.update)
  .delete(AuthMiddleware, FileController.delete);
  

module.exports = router;
