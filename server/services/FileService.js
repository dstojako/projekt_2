const mongoose = require("mongoose");
const File = mongoose.model("File");
const Chunk = mongoose.model("Chunk");

const ObjectId = mongoose.Types.ObjectId;

class FileService {
  static async delete(id) {
    await Chunk.deleteMany({ files_id: id }).exec();
    return await File.deleteOne({ _id: id }).exec();
  }

  

  static async getAll(userId) {
    return await File.aggregate([
      userId ? { $match: { "metadata.owner": userId } } : { $match: {} }
      ,
      { 
        $project: { 
          "_id": 0,
          "id": "$_id",
          "ownerId": { "$toObjectId": "$metadata.owner" },
          "filename": 1,
          "contentType": 1,
          "uploadDate": 1,
          "title": "$metadata.title",
          "school": "$metadata.school",
          "class": "$metadata.class",
          "createDate": 1
        } 
      },
      {
        $lookup:{
          from: "users",
          localField: "ownerId",
          foreignField: "_id",
          as: "user"
        }
      },
      { 
        $project: { 
          "id": 1,
          "filename": 1,
          "contentType": 1,
          "uploadDate": 1,
          "title": 1,
          "school": 1,
          "class": 1,
          "createDate": 1,
          "user": { 
            $first: {
              $map: {
                input: "$user",
                as: "user",
                in: { 
                  id: "$$user._id",
                  firstname: "$$user.firstname",
                  lastname: "$$user.lastname"
                }
              }
            }  
          }
        } 
      },
      {
        $group : { 
          _id : { 
            "school": "$school", 
            "class": "$class"
          },
          data: { 
            $push: "$$ROOT" 
          } 
        }
      },
      { $sort: { "_id.school": -1, "_id.class": 1 } },
      { $project: { _id: 0, data: 1, category: "$_id" } }
    ]).exec();
  }

  static async get(id){
    return await File.findOne({ "_id": id }).exec();
  }

  static async getAllByOwner(ownerId) {
    return await File.find({ "metadata.owner": ownerId }).exec();
  }
}

module.exports = FileService;