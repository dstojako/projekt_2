const UserService = require('../services/UserService');

class AuthController {
  // static async GetByIdAsync(req, res) {
  //   try {
  //     if(!req.user.sub){
  //       return res.status(400)
  //       .json({ statusCode: 400, message: "Bad request" });
  //     }

  //     const user = await UserService.GetByIdAsync(req.user.sub);
  //     if(!user) {
  //       return res.status(404)
  //         .json({ statusCode: 404, message: "Not Found" });
  //     }
  //     return res.status(200).json({statusCode: 200, user});
  //   } 
  //   catch (error) {
  //     console.error(error);
  //     return res.status(500).send('Internal Server Error');
  //   }
  // }
  
  static async register(req, res) 
  {
    var user = await UserService.CreateAsync(req.body);
    if (!user.isValid()) {
      return res.json({ message: "Information about user is not valid", request: req.body});
    }

    try {

      let existingUser = await UserService.GetByEmailAsync(user.email);
      if (existingUser) {
        return res.json({ message: `User with email: ${user.email} already exist`});
      }

      await UserService.SaveAsync(user);
      return res.json(
        { 
          token: user.generateJwt(false) 
        });
    } catch (error) {
      console.log(error);
      return res.json({ message: 'Internal Server Error' });
    }
  }

  static async login(req, res) {
    const { email, password, remember } = req.body;
    try {
      const user = await UserService.GetByEmailAsync(email);
      if (!user || !await user.validPassword(password)) {
        return res.json({ message: "Invalid credentials" });
      }
      return res.json({ token: user.generateJwt(remember) });
    } 
    catch (error) {
      console.log(error);
      return res.json({ message: 'Internal Server Error' });
    }
  }
}

module.exports = AuthController;
