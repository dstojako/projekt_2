const Readable = require("stream").Readable;
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;


let bucket;
mongoose.connection.on("connected", () => {
    bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
});

const FileService = require("../services/FileService");

class FileController {

  static async upload(req, res) {
    const { file } = req.files;
    const metadata = req.body;
    const userId = req.user.id;
    
    if(!file) {
      return res.status(400).json({ message: "File not sent" });
    }
    
    const readableStream = new Readable();
    readableStream.push(file.data);
    readableStream.push(null);

    let uploadStream = bucket.openUploadStream(
      file.name, { 
      contentType: file.mimetype, 
      metadata: { 
        owner: userId,
        ...metadata,
      }
    });

    readableStream.pipe(uploadStream);
    uploadStream.on("error", () => {
        return res.status(500).send("Internal server error");
      });
    uploadStream.on("finish", async () => {
        return res.status(201).json({ message: "File uploaded" });
      });
  }

  static async update(req, res) {
    const { file } = req.files;
    const { fileId } = req.params;
    const userId = req.user.id;
    const metadata = req.body;
    
    if(!file) {
      return res.status(400).json({ message: "File not sent" });
    }

    try {
      await FileService.delete(ObjectId(fileId));
    } catch (error) {
      console.log(error);
      res.status(500).send("Internal server error");
    }
    
    const readableStream = new Readable();
    readableStream.push(file.data);
    readableStream.push(null);

    let uploadStream = bucket.openUploadStream(
      file.name, { 
      contentType: file.mimetype, 
      metadata: { 
        owner: userId,
        ...metadata,
      }
    });

    readableStream.pipe(uploadStream);
    uploadStream.on("error", () => {
        return res.status(500).send("Upload - Internal Server Error");
      });
    uploadStream.on("finish", async () => {
        return res.status(201).json({ message: "File uploaded" });
      });
  }
  
  static async delete(req, res) {
    const { id } = req.params;
    try{
      const file = await FileService.get(ObjectId(id));
      if (!file) {
        return res.status(404).json({ message: "Not found"});
      }
      await FileService.delete(file._id);
      return res.status(204).send();
    } catch (error){
      console.log(error);
      return res.status(500).send("Internal server error");
    }
  }

  static async get(req, res) {
    const { id } = req.params;
    var file;
    try {
      file = await FileService.get(ObjectId(id));
      if(!file) {
        return res.status(404).json({ message: "Not found" });
      }
      return res.status(200).json({ data: file });
    } catch (error) {
      console.log(error);
      return res.status(500).send("Internal server error");
    }
  }
  
  static async getAll(req, res) {
    const { userId } = req.query;

    const files = await FileService.getAll(userId);
    return res.status(200).json({ data: files });
  }

  static async download(req, res) {
    const { id } = req.params;
    var file;

    try {
        file = await FileService.get(id);
    } catch(err) {
        return res.status(500).json({ message: "Internal server error"});
    }

    if(!file) {
       return res.status(404).json({ message: "Not found" });
    }

    let downloadStream = bucket.openDownloadStream(file._id);
    res.writeHead(200, { "Content-Type": file.contentType });
    downloadStream.pipe(res);
  }

}

module.exports = FileController;