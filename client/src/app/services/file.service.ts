import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MathFile } from '../models/file.model';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { TokenService } from './token.service';

const baseUrl = 'http://localhost:3001/api';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private httpOptions = { 
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.tokenService.getToken()}`
    })
  }

  public files$: BehaviorSubject<MathFile[]> = new BehaviorSubject<MathFile[]>([]);
  public user: User | null;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
    private tokenService: TokenService
  ) { 
    this.getFiles();
    this.authService.user$.subscribe(user => this.user = user);
  }

  getFiles(): void {
    this.httpClient.get(`${baseUrl}/files`)
      .pipe(
        map((res: any) => res.data),
        tap(data => this.files$.next(data)),
        tap(x => console.log(this.files$.getValue()))
      ).subscribe();
  }

  getFilesByUser(): Observable<MathFile[]> {
    return this.httpClient.get(`${baseUrl}/files?userId=${this.user?.id}`)
    .pipe(
      map((res: any) => res.data),
      tap(x => console.log(this.files$.getValue()))
    )    
  }

  uploadFile(data: any): Observable<boolean> {
    var formData: FormData = new FormData();
    formData.append('title', data.title);
    formData.append('school', data.school);
    formData.append('class', data.class);
    formData.append('file', data.uploadedFile, data.uploadedFile.name);

    console.log(`Authorization': Bearer ${this.tokenService.getToken()}`);
    
    return this.httpClient.post(`${baseUrl}/files`, formData, this.httpOptions).pipe(map(x => !!x));
  }

  deleteFile(fileId: string): Observable<boolean> {
    return this.httpClient.delete(`${baseUrl}/files/${fileId}`, this.httpOptions).pipe(map(x => !!x));
  }

}
