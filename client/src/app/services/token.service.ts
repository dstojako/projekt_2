import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  public saveToken(token: string): void {
    window.localStorage["token"] = token;
  }

  public getToken(): string | null {
    return window.localStorage.getItem("token");
  }

  public removeToken(): void {
    window.localStorage.removeItem("token");
  }

  public parseToken(token: string): any {
    return JSON.parse(window.atob(token.split('.')[1]));
  }

  public isValidToken(token: string): boolean {
    const payload = this.parseToken(token);
    if (!payload) {
      return false;
    }
    return (payload.exp > Date.now() / 1000);
  }
  
  public getUserFromToken(token: string): User {
    return this.parseToken(token) as User;
  }
}
