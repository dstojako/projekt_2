import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { of, Observable, BehaviorSubject } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

import { TokenService } from './token.service';
import { User } from '../models/user.model';

const baseUrl = 'http://localhost:3001/api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  public isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user$: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  constructor(
    private httpClient: HttpClient,
    private tokenService: TokenService,
  ) { }
  
  register(credentials: any): Observable<boolean> {
    return this.httpClient.post(`${baseUrl}/auth/register`, credentials)
      .pipe(
        map((res: any) => res.token),
        tap(token => this.tokenService.saveToken(token)),
        tap(token => this.setLocalUser(token)),
        map(x => !!x)
      );
  }

  login(credentials: any): Observable<boolean> {
    return this.httpClient.post(`${baseUrl}/auth/login`, credentials)
      .pipe(
        tap(x => console.log(x)),
        map((res: any) => res.token),
        tap(x => console.log(x)),
        tap(token => this.tokenService.saveToken(token)),
        tap(token => this.setLocalUser(token)),
        map(x => !!x)
      );
  }

  setLocalUser(token: string): void {
    const user: User = this.tokenService.getUserFromToken(token);
    this.user$.next(user);
    this.isAuthenticated$.next(true);
  }

  public logout(): Observable<boolean> {
    this.user$.next(null);
    this.isAuthenticated$.next(false);
    this.tokenService.removeToken();
    return of(true);
  }

  public silentLogin(): Observable<boolean> {
    return this.isLoggedIn()
      .pipe(
        filter(x => x !== ''),
        tap(token => this.setLocalUser(token)),
        map(x => !!x)
      );
  }

  public isLoggedIn(): Observable<string> {
    const token = this.tokenService.getToken();
    if (token && this.tokenService.isValidToken(token)) {
      return of(token);
    }
    return of('');
  }

}
