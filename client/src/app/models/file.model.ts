import { User } from "./user.model";

export class MathFile {
  category: MathCategory
  data: MathData[]
}

export class MathCategory {
  school: string;
  class: string;
}

export class MathData {
  id: string;
  filename: string;
  title: string;
  school: string;
  class: string;
  uploadDate: Date;
  contentType: string;
  user: User;

}