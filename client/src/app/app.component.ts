import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public title = 'Math Workbook';
  
  private authSub$: Subscription;
  constructor(
    private authService: AuthService
  ) {
    
  }
  
  ngOnDestroy(): void {
    this.authSub$.unsubscribe();
  }

  ngOnInit(): void {
    this.authSub$ = this.authService.silentLogin().subscribe();
  }
}
