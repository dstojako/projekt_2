import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, pipe, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { MathData, MathFile } from 'src/app/models/file.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public isCollapsedUpload = false;
  public isPrimary = true;
  public fileForm: FormGroup;

  public userFiles: MathFile[];
  public primary: MathFile[];
  public high: MathFile[];
  
  public school = ["Primary School", "High School"]
  public primaryClass = ["1", "2", "3", "4", "5", "6", "7", "8"]
  public highClass = ["1", "2", "3", "4"]

  public isAuthenticated$: Observable<boolean>;
  public user$: Observable<User | null>;
  
  private fileSub$ = new Subject<void>();
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private fileService: FileService
  ) { 
    this.buildForm();
    this.user$ = this.authService.user$;
    this.isAuthenticated$ = this.authService.isAuthenticated$;
    
    this.onChanges();
  }

  ngOnInit(): void {
    this.user$.pipe(takeUntil(this.fileSub$)).subscribe();
    this.isAuthenticated$.pipe(takeUntil(this.fileSub$)).subscribe();
    this.getMyFiles();
  }
  
  ngOnDestroy(): void {
    this.fileSub$.next();
    this.fileSub$.complete();
  }

  onChanges(): void {
    this.fileForm.valueChanges.subscribe(val => {
      this.isPrimary = val.school === "Primary School"
    })
  }

  private buildForm(): void {
    this.fileForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      school: ['', [Validators.required]],
      class: ['', [Validators.required]],
      uploadedFile: [null],
    });
  }

  submitForm(): void {
    console.log(this.fileForm.value);
    
    this.fileService.uploadFile(this.fileForm.value)
      .pipe(
        takeUntil(this.fileSub$),
        switchMap(() => this.fileService.getFilesByUser())
      ).subscribe(files => {
          this.primary = this.getSchool("Primary", files);
          this.high = this.getSchool("High", files);
      });
  }

  onFileChange(event: any) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.fileForm.patchValue({
          uploadedFile: reader.result
        });

      };
    }
  }

  onFileSelect(event: any): void {
    const file = event.target.files[0];
    this.fileForm.get('uploadedFile')!.setValue(file);
  }

  getSchool(type: string, files: MathFile[]): MathFile[] {
    let list = files.filter(x => x.category.school.includes(type));
    console.log(type, list);
    return list;  
  }

  deleteFile(event: any): void {
    this.fileService.deleteFile(event.target.id)
      .pipe(
        takeUntil(this.fileSub$),
        tap(() => this.fileService.getFiles()),
        switchMap(() => this.fileService.getFilesByUser())
      ).subscribe(files => {
        this.primary = this.getSchool("Primary", files);
        this.high = this.getSchool("High", files);
      });
  }

  getMyFiles(): void {
    this.fileService.getFilesByUser()
      .pipe(takeUntil(this.fileSub$))
      .subscribe(files => {
        this.primary = this.getSchool("Primary", files);
        this.high = this.getSchool("High", files);
    });
  }

}




// private fillForm(mathData: MathData) {
  //   this.fileForm.setValue({
  //     title: mathData.title,
  //     school: mathData.school,
  //     class: mathData.class,
  //     uploadedFile: null,
  //   });
  // }