import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MathFile } from 'src/app/models/file.model';
import { FileService } from 'src/app/services/file.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy{
  public fileDownloadUrl = 'http://localhost:3001/api/files/download/';
  
  public files: MathFile[];
  public primary: MathFile[];
  public high: MathFile[];
  
  private unSub$: Subscription;
  constructor(
    private fileService: FileService
  ) { }

  
  ngOnInit(): void {
    this.unSub$ = this.fileService.files$.subscribe(files => {
      this.files = files;
      this.primary = this.getSchool("Primary");
      this.high = this.getSchool("High");
    });
  }

  ngOnDestroy(): void {
    this.unSub$.unsubscribe();
  }

  getSchool(type: string): MathFile[] {
    let list = this.files.filter(x => x.category.school.includes(type));
    console.log(type, list);
    return list;  
  }

  

}
