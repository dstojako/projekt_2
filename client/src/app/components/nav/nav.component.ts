import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy{
  
  public isMenuCollapsed = true;
  public isAuthenticated$: Observable<boolean>;
  public user$: Observable<User | null>;


  @Input() title: string | undefined;
  
  private logoutSub$: Subscription;
  constructor(
    private router: Router,
    private authService: AuthService
  ) { 
    this.user$ = this.authService.user$;
    this.isAuthenticated$ = this.authService.isAuthenticated$;
  }
  
  ngOnInit(): void {
    this.user$.subscribe();
    this.isAuthenticated$.subscribe();
  }
  
  ngOnDestroy(): void {
    this.logoutSub$.unsubscribe();
  }

  logout(): void {
    this.logoutSub$ = this.authService.logout()
      .pipe(tap(() => this.router.navigate([''])))
      .subscribe()
  }

}
